# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: María Torquemada
* Titulación: Doble grado en sistemas de Telecomunicación + ADE
* Cuenta en laboratorios: mariatd
* Cuenta URJC: m.torquemada.2019@alumnos.urjc.es
* Video básico (url): https://youtu.be/REz7UJElLUA
* Video parte opcional (url): https://youtu.be/dQAl-4LZK8k
* Despliegue (url): http://mariatd.pythonanywhere.com/
* Contraseñas: no se necesita
* Cuenta Admin Site: admin/admin

## Resumen parte obligatoria

En esta aplicación web denominada TeVeo vamos a tener 5 páginas por las que ir navegando, la Principal, Cámaras, Configuración, Ayuda y Admin

Página Principal: En la página principal se muestra el listado de comentarios y cada uno con su imagen, texto, fecha y el identificador, los comentarios ordenados de más nuevos a más viejos.

Página de Cámaras: En la página de cámaras se encuentran todas las cámaras con el número de comentarios que tienen. Además disponemos de una página para cada cámara y a su vez cada una tendrá una página dinámica. La página de cada cámara tiene un enlace a la página dinámica y un enlace para poner un comentario. La página dinámica tiene un enlace a la página de cada cámara y un enlace para poner un comentario. Ambas tienen la imagen de la cámara en el momento. 

En las páginas de cada cámara y dinámicas tenemos la opción de poner un comentario, que cuando lo ponemos se muestra la imagen actual de la cámara, el texto, la fecha y la hora.

Dentro de la página de cámaras también podemos ver las cámaras en formato JSON que incluye para cada una de las cámaras toda su información.

Página de Configuración: En la página de configuración hay un formulario para elegir el nombre, tipo de letra y tamaño para que aparezca al poner comentarios.

Página de Ayuda: En la página de ayuda se proporciona una breve explicación de la práctica. He usado un elemento Accordion de bootstrap

Página de Admin: En la página de admin la cual permite crear, editar y eliminar registros de la base de datos y configurar permisos de usuario.

## Lista partes opcionales

* Inclusión de un favicon del sitio.
* Permitir que las sesiones se puedan terminar.
* He incluido algún test unitario.


## Listas blancas
Las imágenes en pythonanywhere no se me descargan correctamente debido a un error en las listas blancas (whitelist), pero en local me van.
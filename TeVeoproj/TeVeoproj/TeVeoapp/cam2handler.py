from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class XMLParser2:
    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = DataHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def get_data(self):
        return self.handler.data_list


class DataHandler(ContentHandler):

    def __init__(self):
        self.is_camera = False
        self.is_content = False
        self.current_content = ""
        self.latitude = ""
        self.longitude = ""
        self.place = ""
        self.url = ""
        self.location = ""
        self.id = ""
        self.data_list = []

    def startElement(self, name, attrs):
        if name == 'cam':
            self.is_camera = True
            self.id = attrs.get('id')
        elif self.is_camera:
            if name == 'url':
                self.is_content = True
            elif name == 'info':
                self.is_content = True
            elif name == 'latitude':
                self.is_content = True
            elif name == 'longitude':
                self.is_content = True

    def endElement(self, name):
        if name == 'cam':
            self.is_camera = False
            self.data_list.append({'src': self.url,
                              'latitude': self.latitude,  # Toma el primer elemento de la latitud
                              'longitude': self.longitude,  # Toma el segundo elemento de la longitud
                              'lugar': self.location,
                              'id': self.id})
        elif self.is_camera:
            if name == 'info':
                self.location = self.current_content
                self.current_content = ""
                self.is_content = False
            elif name == 'latitude':
                self.latitude = self.current_content
                self.current_content = ""
                self.is_content = False
            elif name == 'longitude':
                self.longitude = self.current_content
                self.current_content = ""
                self.is_content = False
            elif name == 'url':
                self.url = self.current_content
                self.current_content = ""
                self.is_content = False

    def characters(self, chars):
        if self.is_content:
            self.current_content += chars

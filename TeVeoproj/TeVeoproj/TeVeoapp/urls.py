from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('camaras/', views.camaras, name="camaras"), #lista
    path('camara/<str:id>/', views.camara, name="camara"),
    path('camaradyn/<str:id>/', views.dinamica, name="dinamica"),
    path('comentario/<str:id>/', views.comentario, name="comentario"),
    path('image_embedded/', views.image_embedded, name='image_embedded'),
    path('comentarios_embedded/', views.comentarios_embedded, name='comentarios_embedded'),
    path('configuracion/', views.config, name="config"),
    path('configuracion/<uuid:token>/', views.config, name='use_auth_token'),
    path('ayuda/', views.ayuda, name="ayuda"),
    path('json/<str:id>/', views.json, name="json"),
]

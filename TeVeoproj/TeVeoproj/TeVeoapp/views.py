from django.shortcuts import render, get_object_or_404
import urllib.request
from .models import Camara, Comentario, Sesion
from django.http import HttpResponse
import string
from .cam1handler import XMLParser
from .cam2handler import XMLParser2
import base64
import random
from django.shortcuts import redirect
from django.utils import timezone
from django.http import HttpResponseRedirect
from django.db.models import Count
from django.http import JsonResponse
import uuid


def generar_session_id(longitud=16):  # Reduce longitud del session_id
    caracteres = string.ascii_letters + string.digits
    return ''.join(random.choices(caracteres, k=longitud))


def index(request):
    session_id = request.session.get("session_id")

    if not session_id or not Sesion.objects.filter(id=session_id).exists():
        # Si no hay un ID de sesión, o no existe una configuración para este ID de sesión,
        # crear uno nuevo y guardar la configuración predeterminada para esta sesión
        session_id = generar_session_id()
        request.session["session_id"] = session_id
        configuracion = Sesion.objects.create(
            id=session_id,
            name="Anónimo",
            size="normal",
            type="Times New Roman"
        )
    else:
        # Obtener la configuración específica de esta sesión
        configuracion = Sesion.objects.get(id=session_id)

    # Obtener los comentarios ordenados de más reciente a más antiguo y todas las cámaras
    comentarios = Comentario.objects.all().order_by("-fecha")
    camaras = Camara.objects.all()

    numb_camaras = camaras.count()
    numb_coments = comentarios.count()

    # Devolver la página con la configuración y otros datos necesarios
    return render(request, "TeVeoapp/index.html", {
        "comentarios": comentarios,
        "camaras": camaras,
        "configuracion": configuracion,
        "numb_camaras": numb_camaras,
        "numb_coments": numb_coments
    })


headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'
}


def download_image(identif):
    """Download image and return it as bytes"""
    try:
        cam = Camara.objects.get(id=identif)
        request = urllib.request.Request(url=cam.URL, headers=headers)
        #print(f"Descargando imagen desde {cam.URL}")  # Mensaje de depuración
        with urllib.request.urlopen(request) as response:
            image = response.read()
    except urllib.error.URLError as e:
        #print(f"Error al descargar la imagen desde {cam.URL}: {e}")
        return None
    return image


def image(request):
    """Return the image as bytes"""
    image = download_image(id)
    if image is None:
        return HttpResponse("No se puede pasar imagen a bytes", status=500)
    else:
        return HttpResponse(image, content_type="image/jpeg")


def image_embedded(request):
    identif = request.GET.get("id")
    if not identif:
        return HttpResponse("ID no proporcionado", status=400)

    image = download_image(identif)
    if image is None:
        return HttpResponse("Error al obtener imagen dinámica", status=500)
    else:
        image_base64 = base64.b64encode(image).decode('utf-8')
        html = f'<img src="data:image/jpeg;base64,{image_base64}">'
        return HttpResponse(html, content_type="text/html")


def comentarios_embedded(request):
    session_id = request.session.get("session_id")

    if not session_id or not Sesion.objects.filter(id=session_id).exists():
        # Crear una nueva sesión si no existe una
        session_id = generar_session_id()
        request.session["session_id"] = session_id
        configuracion = Sesion.objects.create(
            id=session_id,
            name="Anónimo",
            size="normal",
            type="Times New Roman"
        )
    else:
        # Obtener la configuración específica de esta sesión
        configuracion = Sesion.objects.get(id=session_id)

    identif = request.GET.get("id")
    if not identif:
        return HttpResponse("ID no proporcionado", status=400)

    camara = get_object_or_404(Camara, id=identif)
    comentarios = Comentario.objects.filter(camara=camara).order_by("-fecha")

    return render(request, 'TeVeoapp/comentarios_embedded.html', {
        'configuracion': configuracion,
        'comentarios': comentarios,
        'camara': camara
    })


def camaras(request):
    session_id = request.session.get("session_id")

    if not session_id or not Sesion.objects.filter(id=session_id).exists():
        # Si no hay un ID de sesión, o no existe una configuración para este ID de sesión,
        # crear uno nuevo y guardar la configuración predeterminada para esta sesión
        session_id = generar_session_id()
        request.session["session_id"] = session_id
        configuracion = Sesion.objects.create(
            id=session_id,
            name="Anónimo",
            size="normal",
            type="Times New Roman"
        )
    else:
        # Obtener la configuración específica de esta sesión
        configuracion = Sesion.objects.get(id=session_id)

    urls = {
        'Lis1': 'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1.xml',
        'Lis2': 'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado2.xml',
    }

    parsers = {
        'Lis1': XMLParser,
        'Lis2': XMLParser2,
    }

    if request.method == 'POST':
        action = request.POST.get('action')
        if action in urls and action in parsers:
            try:
                stream = urllib.request.urlopen(urls[action])
                parser = parsers[action](stream)
                camaras = parser.get_data()

                for camara_data in camaras:
                    if not Camara.objects.filter(id=camara_data['id']).exists():
                        Camara.objects.create(
                            id=camara_data['id'],
                            name=camara_data['lugar'],
                            latitud=camara_data.get('latitude'),
                            longitud=camara_data.get('longitude'),
                            URL=camara_data['src'].replace(' ', '')
                        )
            except Exception as e:
                print("Error al descargar o procesar el XML:", e)
        # Redirigir después de manejar el POST para evitar reenviar más POSTs
        return redirect("camaras")

    comentarios = Comentario.objects.all()
    camaras = Camara.objects.annotate(num_comentarios=Count('comentario')).order_by('-num_comentarios')

    numb_camaras = camaras.count()
    numb_coments = comentarios.count()

    random_image_url = None
    if camaras:
        random_camera = random.choice(camaras)
        random_image_url = random_camera.URL

    return render(request, "TeVeoapp/camaras.html", {
        "camaras": camaras,
        "configuracion": configuracion,
        "numb_camaras": numb_camaras,
        "numb_coments": numb_coments,
        "random_image_url": random_image_url,  # Pasar la URL de la cámara aleatoria
    })


def camara(request, id):
    session_id = request.session.get("session_id")

    if not session_id or not Sesion.objects.filter(id=session_id).exists():
        # Crear una nueva sesión si no existe una
        session_id = generar_session_id()
        request.session["session_id"] = session_id
        configuracion = Sesion.objects.create(
            id=session_id,
            name="Anónimo",
            size="normal",
            type="Times New Roman"
        )
    else:
        # Obtener la configuración específica de esta sesión
        configuracion = Sesion.objects.get(id=session_id)

    camara = get_object_or_404(Camara, id=id)
    comentarios = Comentario.objects.filter(camara=camara).order_by("-fecha")
    camaras = Camara.objects.all()

    numb_camaras = camaras.count()
    numb_coments = Comentario.objects.count()

    return render(request, 'TeVeoapp/camara.html', {
        "configuracion": configuracion,
        'camara': camara,
        'comentarios': comentarios,
        "numb_camaras": numb_camaras,
        "numb_coments": numb_coments,
    })


def dinamica(request, id):
    session_id = request.session.get("session_id")

    if not session_id or not Sesion.objects.filter(id=session_id).exists():
        # Crear una nueva sesión si no existe una
        session_id = generar_session_id()
        request.session["session_id"] = session_id
        configuracion = Sesion.objects.create(
            id=session_id,
            name="Anónimo",
            size="normal",
            type="Times New Roman"
        )
    else:
        # Obtener la configuración específica de esta sesión
        configuracion = Sesion.objects.get(id=session_id)

    camara = get_object_or_404(Camara, id=id)
    comentarios = Comentario.objects.filter(camara=camara).order_by("-fecha")

    numb_camaras = Camara.objects.count()
    numb_coments = Comentario.objects.count()

    return render(request, 'TeVeoapp/camaradyn.html', {
        "configuracion": configuracion,
        'camara': camara,
        'comentarios': comentarios,
        "numb_camaras": numb_camaras,
        "numb_coments": numb_coments,
    })


def comentario(request, id):
    session_id = request.session.get("session_id")
    if not session_id:
        session_id = generar_session_id()
        request.session["session_id"] = session_id
        Sesion.objects.create(
            id=session_id,
            name="Anónimo",
            size="normal",
            type="Times New Roman"
        )
    else:
        if not Sesion.objects.filter(id=session_id).exists():
            Sesion.objects.create(
                id=session_id,
                name="Anónimo",
                size="normal",
                type="Times New Roman"
            )

    camara = get_object_or_404(Camara, id=id)

    camaras = Camara.objects.all()

    numb_camaras = camaras.count()
    numb_coments = Comentario.objects.count()

    configuracion = Sesion.objects.get(id=session_id)

    if request.method == 'POST':
        comentario_texto = request.POST.get('comentario')
        comentario = Comentario.objects.create(
            camara=camara,
            usuario=session_id,  # Utilizar el ID de la sesión como identificador del usuario
            usuario_nombre=configuracion.name,  # Guardar el nombre actual del usuario
            text=comentario_texto,
            fecha=timezone.now()
        )

        imagen_camara = download_image(camara.id)
        if imagen_camara:
            comentario.img = base64.b64encode(imagen_camara).decode('utf-8')
            comentario.save()

        return HttpResponseRedirect('/camaradyn/{0}/'.format(id))
    else:
        imagen_camara = camara.URL
        fecha_hora_actual = timezone.now()

        return render(request, "TeVeoapp/comentario.html", {
            "camara": camara,
            "imagen_camara": imagen_camara,
            "fecha_hora": fecha_hora_actual,
            "configuracion": configuracion,
            "numb_camaras": numb_camaras,
            "numb_coments": numb_coments
        })


def config(request, token=None):
    # Si se proporciona un token, cargar la configuración correspondiente
    if token:
        try:
            configuracion = Sesion.objects.get(auth_token=token)
            request.session["session_id"] = configuracion.id
            return redirect("config")
        except Sesion.DoesNotExist:
            return redirect("config")

    # Obtener el ID de sesión desde la sesión del navegador
    session_id = request.session.get("session_id")
    if not session_id:
        session_id = generar_session_id()
        request.session["session_id"] = session_id
        Sesion.objects.create(
            id=session_id,
            name="Anónimo",
            size="normal",
            type="Times New Roman"
        )
    else:
        if not Sesion.objects.filter(id=session_id).exists():
            Sesion.objects.create(
                id=session_id,
                name="Anónimo",
                size="normal",
                type="Times New Roman"
            )

    configuracion = Sesion.objects.get(id=session_id)

    if request.method == 'POST':
        if 'authorize_link' in request.POST:
            # Generar un identificador único para la autorización
            auth_token = str(uuid.uuid4())
            # Guardar el token en la sesión
            configuracion.auth_token = auth_token
            configuracion.save()
            # Generar la URL de autorización
            authorize_url = request.build_absolute_uri(f'/configuracion/{auth_token}/')
            return render(request, 'TeVeoapp/config.html', {
                'configuracion': configuracion,
                'size_choices': [('small', 'Pequeño'), ('normal', 'Normal'), ('large', 'Grande')],
                'type_choices': [('Arial', 'Arial'), ('Times New Roman', 'Times New Roman'), ('Impact', 'Impact')],
                'authorize_url': authorize_url,
                'numb_camaras': Camara.objects.count(),
                'numb_coments': Comentario.objects.count()
            })

        elif 'terminar_sesion' in request.POST:
            # Terminar la sesión actual y restaurar la configuración predeterminada
            configuracion.name = "Anónimo"
            configuracion.size = "normal"
            configuracion.type = "Times New Roman"
            configuracion.auth_token = None  # Eliminar cualquier token de autorización existente
            configuracion.save()
            request.session.flush()  # Limpiar completamente la sesión

            # Crear una nueva sesión y redirigir a la página de configuración
            session_id = generar_session_id()
            request.session["session_id"] = session_id
            Sesion.objects.create(
                id=session_id,
                name="Anónimo",
                size="normal",
                type="Times New Roman"
            )
            return redirect("index")  # Redirige a la principal al terminar sesión

        # Manejo de la actualización de configuración
        name = request.POST.get('name')
        size = request.POST.get('size')
        type = request.POST.get('type')

        configuracion.name = name
        configuracion.size = size
        configuracion.type = type
        configuracion.save()
        return redirect("config")

    numb_camaras = Camara.objects.count()
    numb_coments = Comentario.objects.count()

    return render(request, "TeVeoapp/config.html", {
        "configuracion": configuracion,
        "size_choices": [('small', 'Pequeño'), ('normal', 'Normal'), ('large', 'Grande')],
        "type_choices": [('Arial', 'Arial'), ('Times New Roman', 'Times New Roman'), ('Impact', 'Impact')],
        "numb_camaras": numb_camaras,
        "numb_coments": numb_coments
    })


def ayuda(request):
    session_id = request.session.get("session_id")

    if not session_id or not Sesion.objects.filter(id=session_id).exists():
        # Crear una nueva sesión si no existe una
        session_id = generar_session_id()
        request.session["session_id"] = session_id
        configuracion = Sesion.objects.create(
            id=session_id,
            name="Anónimo",
            size="normal",
            type="Times New Roman"
        )
    else:
        # Obtener la configuración específica de esta sesión
        configuracion = Sesion.objects.get(id=session_id)

    numb_camaras = Camara.objects.count()
    numb_coments = Comentario.objects.count()

    return render(request, "TeVeoapp/ayuda.html", {
        "configuracion": configuracion,
        "numb_camaras": numb_camaras,
        "numb_coments": numb_coments
    })


def json(request, id):
    camara = get_object_or_404(Camara, id=id)
    num_comentarios = Comentario.objects.filter(camara=camara).count()

    camara_data = {
        "id": camara.id,
        "nombre": camara.name,
        "latitud": camara.latitud,
        "longitud": camara.longitud,
        "URL": camara.URL,
        "num_comentarios": num_comentarios
    }

    return JsonResponse(camara_data)


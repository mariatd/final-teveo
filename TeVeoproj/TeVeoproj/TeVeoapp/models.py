# Create your models here.
from django.db import models


class Sesion(models.Model):
    name = models.CharField(max_length=128, default='Anónimo')
    id = models.CharField(max_length=256, primary_key=True)
    size = models.CharField(max_length=64, default='normal')
    type = models.CharField(max_length=128, default='Times New Roman')
    auth_token = models.CharField(max_length=100, blank=True, null=True)


class Camara(models.Model):
    id = models.CharField(max_length=128, primary_key=True)
    name = models.CharField(max_length=256)
    latitud = models.CharField(max_length=128)
    longitud = models.CharField(max_length=128)
    URL = models.URLField(max_length=256)


# Relación  de cada cámara 1 : N comentario
class Comentario(models.Model):
    camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    usuario = models.CharField(max_length=255)  # ID o identificador del usuario
    usuario_nombre = models.CharField(max_length=255, default="Anónimo")
    text = models.TextField()
    fecha = models.DateTimeField(auto_now_add=True)
    img = models.TextField(blank=True, null=True)  # Para la imagen base64 opcional

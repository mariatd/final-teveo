from django.contrib import admin
from .models import Camara, Comentario, Sesion

admin.site.register(Camara)
admin.site.register(Comentario)
admin.site.register(Sesion)


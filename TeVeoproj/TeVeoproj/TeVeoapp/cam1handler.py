from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class XMLParser:
    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = DataHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def get_data(self):
        return self.handler.data_list


class DataHandler(ContentHandler):

    def __init__(self):
        self.is_camera = False
        self.is_content = False
        self.current_content = ""
        self.location = ""
        self.coordinates = ""
        self.id = ""
        self.source = ""
        self.data_list = []

    def startElement(self, name, attrs):
        if name == 'camara':
            self.is_camera = True
        elif self.is_camera:
            if name == 'lugar':
                self.is_content = True
            elif name == 'coordenadas':
                self.is_content = True
            elif name == 'src':
                self.is_content = True
            elif name == 'id':
                self.is_content = True

    def endElement(self, name):
        if name == 'camara':
            self.is_camera = False
            self.data_list.append({'src': self.source,
                              'latitude': self.coordinates.split(',')[0],  # Obtener la latitud
                              'longitude': self.coordinates.split(',')[1],  # Obtener la longitud
                              'lugar': self.location,
                              'id': self.id})
        elif self.is_camera:
            if name == 'lugar':
                self.location = self.current_content
                self.current_content = ""
                self.is_content = False
            elif name == 'id':
                self.id = self.current_content
                self.current_content = ""
                self.is_content = False
            elif name == 'coordenadas':
                self.coordinates = self.current_content
                self.current_content = ""
                self.is_content = False
            elif name == 'src':
                self.source = self.current_content
                self.current_content = ""
                self.is_content = False

    def characters(self, chars):
        if self.is_content:
            self.current_content += chars

from django.test import TestCase
from django.test import Client
from .models import Camara, Sesion
from django.urls import reverse
from .views import generar_session_id, image_embedded
from django.http import HttpRequest, HttpResponse


class IndexTest(TestCase):
    def test_index_page(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'TeVeoapp/index.html')


class AyudaTest(TestCase):
    def test_ayuda(self):
        c = Client()
        response = c.get('/ayuda/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'TeVeoapp/ayuda.html')


class CamdynTest(TestCase):
    def test_camdyn(self):
        c = Client()
        camara = Camara.objects.create(id='1', name='Cuzco', latitud='2.0', longitud='5.0',
                                       URL='https://ctraficomovilidad.malaga.eu/recursos/movilidad/camaras_trafico/TV-24.jpg')
        response = c.get(f'/camaradyn/{camara.id}/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'TeVeoapp/camaradyn.html')


class ConfigTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.session = Sesion.objects.create(id='abcdefg123456789', name='Maria', size='normal', type='Times New Roman')

    def test_config(self):
        # Establecer la sesión en el cliente de prueba
        self.client.session['session_id'] = self.session.id
        self.client.session.save()

        # Realizar una solicitud GET a la vista 'config' con un token simulado
        response = self.client.get(reverse('config'))

        # Verificar que la respuesta sea exitosa (código de estado 200) o que termine en un redirección (código de estado 302)
        self.assertIn(response.status_code, [200, 302])

        # Si la respuesta es una redirección (código de estado 302), verificar que la redirección sea a la página de inicio
        if response.status_code == 302:
            self.assertEqual(response.url, reverse('config'))

        # Verificar que se haya utilizado la plantilla 'TeVeoapp/config.html' si no es una redirección
        if response.status_code == 200:
            self.assertTemplateUsed(response, 'TeVeoapp/config.html')


class CamarasTest(TestCase):
    def test_camaras(self):
        c = Client()
        response = c.get('/camaras/')
        content = response.content.decode('utf-8')
        self.assertIn("<p>No hay imágenes disponibles</p>", content)


class CamaraTest(TestCase):
    def test_camara(self):
        c = Client()
        camara = Camara.objects.create(id='1', name='Cuzco', latitud='2.0', longitud='5.0',
                                       URL='https://ctraficomovilidad.malaga.eu/recursos/movilidad/camaras_trafico/TV-24.jpg')
        response = c.get(f'/camara/{camara.id}/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'TeVeoapp/camara.html')


class ComentarioTest(TestCase):
    def test_comentario(self):
        c = Client()
        camara = Camara.objects.create(id='2', name='Principe de Vergara', latitud='2.0', longitud='5.0',
                                       URL='https://ctraficomovilidad.malaga.eu/recursos/movilidad/camaras_trafico/TV-24.jpg')
        response = c.get(f'/comentario/{camara.id}/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'TeVeoapp/comentario.html')


class JsonTest(TestCase):
    def test_json(self):
        c = Client()
        camara = Camara.objects.create(id='B', name='Malaga', latitud='1.0', longitud='3.0',
                                       URL='https://ctraficomovilidad.malaga.eu/recursos/movilidad/camaras_trafico/TV-24.jpg')
        response = c.get(f'/json/{camara.id}/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')
        # Verificar que la respuesta JSON tiene los datos esperados
        json_response = response.json()
        self.assertEqual(json_response['id'], 'B')


class ComentarioEmbTest(TestCase):
    def test_comenemb(self):
        c = Client()
        camara = Camara.objects.create(id='3', name='Principe de Vergara', latitud='3.0', longitud='2.0',
            URL='https://ctraficomovilidad.malaga.eu/recursos/movilidad/camaras_trafico/TV-24.jpg')
        url = reverse('comentarios_embedded') + f'?id={camara.id}'
        response = c.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'TeVeoapp/comentarios_embedded.html')


class ImagenEmbTest(TestCase):
    def test_imgemb(self):
        c = Client()
        camara = Camara.objects.create(id='A', name='Cuzco', latitud='4.0', longitud='4.0',
                                       URL='https://ctraficomovilidad.malaga.eu/recursos/movilidad/camaras_trafico/TV-24.jpg')
        url = reverse('image_embedded') + f'?id={camara.id}'
        response = c.get(url)
        content = response.content.decode('utf-8')
        self.assertIn("<img src=", content)


# TEST UNITARIOS


class TestGenerarSessionId(TestCase):
    def test_longitud_por_defecto(self):
        session_id = generar_session_id()
        self.assertEqual(len(session_id), 16)

    def test_longitud_especifica(self):
        longitud_esperada = 20  # Nueva longitud del id de sesión
        session_id = generar_session_id(longitud_esperada)
        self.assertEqual(len(session_id), longitud_esperada)  # Comprobación al cambiar longitud de 16 a 20


class ImageEmbeddedTestCase(TestCase):
    def setUp(self):
        # Crear una cámara válida para usar su ID en las pruebas
        self.camara = Camara.objects.create(id='1',name='Cámara de Prueba',latitud='2.0', longitud='5.0',
            URL='https://ctraficomovilidad.malaga.eu/recursos/movilidad/camaras_trafico/TV-24.jpg')
        # Crear una sesión válida en la base de datos para simular la existencia previa de sesión
        self.session_id = generar_session_id()

    def test_image_embedded_success(self):
        # Simular una solicitud GET con un ID válido
        request = HttpRequest()
        request.GET['id'] = self.camara.id
        request.session = {'session_id': self.session_id}

        # Llamar a la vista image_embedded con la solicitud simulada
        response = image_embedded(request)

        # Verificar que la respuesta sea un objeto HttpResponse
        self.assertIsInstance(response, HttpResponse)

        # Verificar que la respuesta contenga la etiqueta <img>
        self.assertIn('<img', response.content.decode('utf-8'))

